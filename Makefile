# Set the image name
IMAGE_NAME=httpserver

# Set the container name
CONTAINER_NAME=httpserver-container

# Default make target
all: build run

# Build the Docker image
build:
	docker build -t $(IMAGE_NAME) .

# Run the Docker container
run:
	docker run -d --name $(CONTAINER_NAME) -p 8080:8080 $(IMAGE_NAME)

# Stop and remove the Docker container
clean:
	docker stop $(CONTAINER_NAME)
	docker rm $(CONTAINER_NAME)

# Remove the Docker image
clean-image:
	docker rmi $(IMAGE_NAME)

helm:
	helm upgrade --install httpserver ./chart

helm-clean:
	helm delete httpserver
