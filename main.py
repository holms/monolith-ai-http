from http.server import BaseHTTPRequestHandler, HTTPServer

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        # Send response status code
        self.send_response(200)

        # Send headers
        self.send_header('Content-type', 'text/plain')
        self.end_headers()

        # Send the response body
        self.wfile.write(b"monolith")

# Server settings
host = '0.0.0.0'
port = 8080
server_address = (host, port)

# Create HTTP server
httpd = HTTPServer(server_address, SimpleHTTPRequestHandler)
print(f'Serving HTTP on {host} port {port} (http://{host}:{port}/) ...')

# Run the server
httpd.serve_forever()
