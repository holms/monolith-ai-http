# azure

variable "name" {
  description = "Unique name for AKS cluster"
  type        = string
  default     = "monolith"
}

variable "location" {
  description = "Azure region for the resources"
  default     = "North Europe"
}

variable "subscription_id" {
  description = "Azure Subscription ID"
  default     = "8657deed-aa21-4954-a321-efec7fae0bf9"
}

variable "principal_id" {
  description = "The Principal ID of the user or service principal to assign the role to"
  type        = string
  default     = "4cc03aea-8bcb-457c-88a0-24158e0edaac"
}
