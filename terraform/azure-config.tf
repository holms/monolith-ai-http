resource "azurerm_resource_group" "monolith" {
  name     = "monolith"
  location = var.location
}

resource "azurerm_role_assignment" "contributor" {
  scope                = azurerm_resource_group.monolith.id
  role_definition_name = "Contributor"
  principal_id         = var.principal_id
}


