resource "azurerm_kubernetes_cluster" "aks" {
  name                = "monolith-${var.name}"
  location            = azurerm_resource_group.monolith.location
  resource_group_name = azurerm_resource_group.monolith.name
  dns_prefix          = "monolith-${var.name}"

  default_node_pool {
    name           = "default"
    node_count     = 1
    vm_size        = "Standard_B2s"
    vnet_subnet_id = azurerm_subnet.aks_subnet.id
  }

  identity {
    type = "SystemAssigned"
  }
}
