FROM python:3.10-alpine

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy all files from the current directory to the container
COPY . .

# Expose the port the server is listening on
EXPOSE 8080

# Command to run the server
CMD ["python3", "main.py"]
