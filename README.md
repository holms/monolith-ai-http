# Monolith httpserver

## Run docker locally

```make```

## Kill docker

```make clean```

## Deploy aks cluster to azure

* Set your azure subscription id and principal id in `variables.tf`
* Copy `backend.tf.example` as `backend.tf`
* Change gitlab_username to your gitlab usernam in `backend.tf`
* Create your personal gitlab token and change gitlab_password variable in `backend.tf`
* Go to `gitlab>project>operate>terraform state>terraform init configuration` copy project id to `backend.tf`
* Initialize terraform

```
terraform init
```

* Deploy the resources

```
terraform apply
```

## Provision k8s cluster

* Install `azcli`
* Run `az login`
* Get k8s configuration for `kubectl`:

```
az aks get-credentials --name monolith-monolith --resource-group monolith --admin
kubectl config use-context monolith-monolith-admin
```

* Create access token in repo settings and select `read_registry` permission
* Deploy gitlab secret:

```
kubectl create secret docker-registry gitlab-registry \
    --docker-server=registry.gitlab.com \
    --docker-username=<your_username> \
    --docker-password=<your_access_toke> \
    --docker-email=<your_email>

```

## Install Helm Chart

Install `helm` and run:

```
helm upgrade --install httpserver ./chart
```
